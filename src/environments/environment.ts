// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/slim/',
  firebase:{
    apiKey: "AIzaSyCDdA0FYb1fKxxUn-nyLUz0USOrkl-i684",
    authDomain: "products-45244.firebaseapp.com",
    databaseURL: "https://products-45244.firebaseio.com",
    projectId: "products-45244",
    storageBucket: "products-45244.appspot.com",
    messagingSenderId: "576807122137"
  }
};
