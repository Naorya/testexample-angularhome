import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
  
})
export class UsersComponent implements OnInit {

  users;
  usersKeys =[];


  constructor(private service:UsersService, private spinnerService: Ng4LoadingSpinnerService) {
    //let service = new UsersService();
    //this.spinnerService.show();
    service.getUsers().subscribe(response=>{
      //console.log(response.json());
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
      //this.spinnerService.hide();
    });
   }
   optimisticAdd(name){//user= $event <<------מהאי טי מל
    console.log("addName worked " + name);
    var newKey =  parseInt(this. usersKeys[this.usersKeys.length-1],0) +1;
    var newUserObject ={};//מגדיר שזה אובייקט כי זה מערך של אובייקטים
    newUserObject['name'] = name;
    this.users[newKey] = newUserObject;
    this.usersKeys = Object.keys(this.users);//לוקח את מסאג' קייז ומוסיף לו את האיבר שהוספנו במערך
      
  }
   deleteUser(key){
    console.log.apply(key);
    let index = this.usersKeys.indexOf(key);
    this.usersKeys.splice(index,1);
    //delete from server
    this.service.deleteUser(key).subscribe(
      response=>console.log(response)
    );
  }
  
   
  pasemisticAdd(){
    this.service.getUsers().subscribe(response=>{
          console.log(response.json())//arrow function. .json() converts the string that we recieved to json
          this.users = response.json();
          this.usersKeys = Object.keys(this.users);
        
      });
        
    }  
  ngOnInit() {
  }

}