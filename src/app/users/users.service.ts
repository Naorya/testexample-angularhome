import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/Rx';
@Injectable()
export class UsersService {
  http:Http;
  
  getUsersFire() {
        
        return this.db.list('/users').valueChanges();
     }
  postUser(data){//השיטה תקבל קובץ גייסון ותחליף אותו
    let options =  {
      headers:new Headers({//הגדרנו דרך מחלקה מיוחדת של אנגולר  שנקראת הדרס שליחה של קי וואליו
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('phone',data.phone).append('city',data.city);//פאראמס הוא למעשה מבנה נתונים שמחזיק קי ו- ואליו, קי הוא המאסג' והואליו הו א הדאטא.מסאג
     return this.http.post(environment.url+'users',params.toString(),options);
  }
  getUser(id){
    
    let response =  this.http.get(environment.url+'users/'+id);
    return response;
  }

  deleteUser (key){
    return this.http.delete(environment.url+'users/'+key)
  }
  putUser(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('phone',data.phone).append('city',data.city);
    return this.http.put(environment.url+'users/'+ key,params.toString(), options);
  }
  
  getUsers(){
    //return ['a','b','c'];
    //get users from the SLIM rest API (Don't say DB)
    return this.http.get(environment.url+'users');
  }
  
  constructor(http:Http,private db:AngularFireDatabase) { 
    this.http = http;
  }
}