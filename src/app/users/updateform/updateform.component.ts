import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { UsersService } from './../users.service';
import {FormGroup , FormControl} from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';


@Component({
  selector: 'updateform',
  templateUrl: './updateform.component.html',
  styleUrls: ['./updateform.component.css']
})
export class UpdateformComponent implements OnInit {


  @Output() addName:EventEmitter <any> = new EventEmitter <any>(); // -מגדירים את שם המשתנה בשם אווטפוט מסוג איונט אמיטר כאשר מבצעים השמה של אמיטר חדש ובכך בנינו תשתית של העברת מידע מהבן שהוא מסאג' פורם לאב שהוא מסאגס
  @Output() addNamePs:EventEmitter <any> = new EventEmitter <any>(); //pasemistic event

  service:UsersService;
  addname = new FormGroup({//בנייה של מבנה נתונים בקוד שמתאים לטופס
      name:new FormControl(""),
      phone:new FormControl(""),
      city:new FormControl("")
  });

  sendData(){
    this.addName.emit(this.addname.value.name);

    console.log(this.addname.value);
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putUser(this.addname.value, id).subscribe(
        response => {
          console.log(response.json());
          this.addNamePs.emit();
          this.router.navigateByUrl("/")//חזרה לדף הבית
        }
      );
    })
    
  }
  constructor(service:UsersService, private route:ActivatedRoute,private router:Router) {
    this.service = service;

   }

   user;
   ngOnInit() {
     this.route.paramMap.subscribe(params=>{
       let id = params.get('id');
       console.log(id);
       this.service.getUser(id).subscribe(response=>{
         this.user = response.json();
         console.log(this.user);
       })
   })
   }
   
   }