import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { UsersService } from './users/users.service';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AngularFireDatabase } from 'angularfire2/database';
import { HttpModule } from '@angular/http';


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';
import { AddnamesComponent } from './users/addnames/addnames.component';
import { UpdateformComponent } from './users/updateform/updateform.component';
import { UsersfComponent } from './usersf/usersf.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NavigationComponent,
    NotFoundComponent,
    AddnamesComponent,
    UpdateformComponent,
    UsersfComponent
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase), 
    AngularFireDatabaseModule,
    FormsModule,
    ReactiveFormsModule,
    Ng4LoadingSpinnerModule.forRoot(),
    RouterModule.forRoot([
      {path:'',component:UsersComponent},//ריק כי מציין את דף הבית
      {path:'users',component:UsersComponent},
      {path:'updateform/:id', component:UpdateformComponent},
      {path:'products', component:ProductsComponent},
      {path:'usersf', component:UsersfComponent},
      {path:'**',component:NotFoundComponent}
    ])
  ],
  providers: [
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
