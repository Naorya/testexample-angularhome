import { Component, OnInit } from '@angular/core';
import { UsersService } from './../users/users.service';
@Component({
  selector: 'usersf',
  templateUrl: './usersf.component.html',
  styleUrls: ['./usersf.component.css']
})
export class UsersfComponent implements OnInit {

  users;
  
    constructor(private service:UsersService) { }

    ngOnInit() {
      this.service.getUsersFire().subscribe(response=>
       {console.log(response);
          this.users= response;
     
        })
    }
  }