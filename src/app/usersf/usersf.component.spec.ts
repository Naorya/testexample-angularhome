import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersfComponent } from './usersf.component';

describe('UsersfComponent', () => {
  let component: UsersfComponent;
  let fixture: ComponentFixture<UsersfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
